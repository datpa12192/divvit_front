import React from "react";
import {TextField, MenuItem} from "@material-ui/core";
import FunctionService from "./FunctionService";
import ButtonBack from "../Button/ButtonBack";
import ButtonContinue from "../Button/ButtonContinue";

export default class Form1AboutYourSelf extends React.Component {

  state = {
    functions: []
  };

  componentDidMount() {
    new FunctionService().getList().then(functions => {
      this.setState({functions: functions});
    });
  }

  render() {
    const {formData, activeStep, classes, handleBack, handleSubmit, handleChangeData, errors} = this.props;
    const functions = this.state.functions;

    return (
      <form autoComplete="off">
        <div>
          <TextField
            id="outlined-full-width"
            label="Your Name"
            error={typeof errors.name != "undefined" ? errors.name.type : false}
            helperText={typeof errors.name != "undefined" ? errors.name.message : false}
            required
            style={{margin: 8}}
            placeholder="Input your name"
            fullWidth
            name="name"
            value={formData.name}
            onChange={handleChangeData}
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            variant="outlined"
          />
        </div>

        <div>
          <TextField
            id="outlined-full-width"
            label="Your Email"
            style={{margin: 8}}
            placeholder="Input your email"
            error={typeof errors.email != "undefined" ? errors.email.type : false}
            helperText={typeof errors.email != "undefined" ? errors.email.message : false}
            required
            fullWidth
            value={formData.email}
            onChange={handleChangeData}
            name="email"
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            variant="outlined"
          /></div>

        <div>
          <TextField
            id="outlined-select-function"
            select
            style={{margin: 8}}
            fullWidth
            required
            label="Your Function"
            name="function"
            error={typeof errors.function != "undefined" ? errors.function.type : false}
            helperText={typeof errors.function != "undefined" ? errors.function.message : false}
            value={formData.function}
            onChange={handleChangeData}
            variant="outlined"
          >
            {functions.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>

          <ButtonBack
            activeStep={activeStep}
            classes={classes}
            handleBack={handleBack}
          />

          <ButtonContinue
            activeStep={activeStep}
            classes={classes}
            handleSubmit={handleSubmit}
          />
        </div>
      </form>
    );
  }
}
