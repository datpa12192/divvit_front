export default class UserService{

  sendData(user) {
    return fetch(`http://172.16.10.170:8080/user`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"email": user.email, "function": user.function, "name": user.name})
    });
  }
}
