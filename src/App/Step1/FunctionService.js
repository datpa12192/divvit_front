export default class FunctionService {
  async getList() {
    return fetch(`http://172.16.10.170:8080/functionList`)
      .then(response => response.json())
      .then(data => data.map(function (value, label) {
        return {
          value: Object.keys(value)[0],
          label: value[Object.keys(value)[0]]
        }
      }))
  }
}
