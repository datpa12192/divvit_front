import React from "react";
import {Button} from "@material-ui/core";

class ButtonBack extends React.Component {

  render() {
    const {activeStep, classes, handleBack} = this.props;

    return (
      <Button
        disabled={activeStep === 0}
        className={classes.button}
        onClick={handleBack}
      >
        Back
      </Button>
    );
  }
};

export default ButtonBack;
