import React from "react";
import {Button} from "@material-ui/core";

class ButtonContinue extends React.Component{

  render() {
    const {activeStep, classes, handleSubmit} = this.props;

    return(
      <Button
        style={{margin: 8, float: "right"}}
        variant="contained"
        color="primary"
        className={classes.button}
        onClick={handleSubmit}
      >
        {activeStep === 2 ? 'Finish' : 'Next'}
      </Button>
    );
  }
}

export default ButtonContinue;
