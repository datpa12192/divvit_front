import React from "react";

export default class Form2Validation extends React.Component {

  isValidUrl(url) {
    const urlRegex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;

    return !urlRegex.test(url)
  }

  validation(formData) {
    const errors = {};

    if (formData.shop_name === '') {
      errors.shop_name = {
        type: true,
        message: 'Required'
      }
    } else {
      delete errors.shop_name;
    }

    if (formData.shop_url === '') {
      errors.shop_url = {
        type: true,
        message: 'Required'
      }
    } else if (this.isValidUrl(formData.shop_url)) {
      errors.shop_url = {
        type: true,
        message: 'Not is URL'
      }
    } else {
      delete errors.shop_url
    }

    if (formData.currency === '') {
      errors.currency = {
        type: true,
        message: 'Required'
      }
    } else {
      delete errors.currency;
    }

    if (formData.language === '') {
      errors.language = {
        type: true,
        message: 'Required'
      }
    } else {
      delete errors.language;
    }

    if (formData.timezone === '') {
      errors.timezone = {
        type: true,
        message: 'Required'
      }
    } else {
      delete errors.timezone;
    }

    return errors;
  }
}
