import React from "react";
import {TextField, MenuItem} from "@material-ui/core";
import CurrencyService from "./CurrencyService";
import TimezoneService from "./TimezoneService";
import ButtonBack from "../Button/ButtonBack";
import ButtonContinue from "../Button/ButtonContinue";
import LanguageService from "./LanguageService";

export default class Form2AboutYourShop extends React.Component {

  state = {
    currencies: [],
    timezones: [],
    languages: []
  };

  componentDidMount() {
    new CurrencyService().getList().then(currencies => {
      this.setState({currencies: currencies});
    });

    new TimezoneService().getList().then(timezones => {
      this.setState({timezones: timezones});
    });

    new LanguageService().getList().then(languages => {
      this.setState({languages: languages});
    });
  }

  render() {
    const {formData, activeStep, classes, handleBack, handleSubmit, handleChangeData, errors} = this.props;
    const {currencies, timezones, languages} = this.state;

    return (
      <form action="">
        <div>
          <TextField
            id="outlined-full-width"
            label="Name"
            style={{margin: 8}}
            placeholder="Shop's Name"
            required
            error={typeof errors.shop_name != "undefined" ? errors.shop_name.type : false}
            helperText={typeof errors.shop_name != "undefined" ? errors.shop_name.message : false}
            fullWidth
            name="shop_name"
            value={formData.shop_name}
            onChange={handleChangeData}
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            variant="outlined"
          />
        </div>

        <div>
          <TextField
            id="outlined-full-width"
            label="URL"
            style={{margin: 8}}
            placeholder="Shop's URL"
            name="shop_url"
            error={typeof errors.shop_url != "undefined" ? errors.shop_url.type : false}
            helperText={typeof errors.shop_url != "undefined" ? errors.shop_url.message : 'Ex: http://gem.com'}
            value={formData.shop_url}
            onChange={handleChangeData}
            required
            fullWidth
            margin="normal"
            InputLabelProps={{
              shrink: true,
            }}
            variant="outlined"
          />
        </div>

        <div>
          <TextField
            id="outlined-select-currency"
            select
            style={{margin: 8}}
            fullWidth
            label="Currency"
            name="currency"
            required
            error={typeof errors.currency != "undefined" ? errors.currency.type : false}
            helperText={typeof errors.currency != "undefined" ? errors.currency.message : false}
            value={formData.currency}
            onChange={handleChangeData}
            variant="outlined"
          >
            {currencies.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>

        <div>
          <TextField
            id="outlined-select-timezone"
            select
            style={{margin: 8}}
            fullWidth
            label="Timezone"
            name="timezone"
            value={formData.timezone}
            onChange={handleChangeData}
            required
            error={typeof errors.timezone != "undefined" ? errors.timezone.type : false}
            helperText={typeof errors.timezone != "undefined" ? errors.timezone.message : false}
            variant="outlined"
          >
            {timezones.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>

        <div>
          <TextField
            id="outlined-select-language"
            select
            style={{margin: 8}}
            fullWidth
            label="Language"
            name="language"
            value={formData.language}
            onChange={handleChangeData}
            required
            error={typeof errors.language != "undefined" ? errors.language.type : false}
            helperText={typeof errors.language != "undefined" ? errors.language.message : false}
            variant="outlined"
          >
            {languages.map(option => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>

        <ButtonBack
          activeStep={activeStep}
          classes={classes}
          handleBack={handleBack}
        />

        <ButtonContinue
          activeStep={activeStep}
          classes={classes}
          handleSubmit={handleSubmit}
        />
      </form>
    );
  }
}
