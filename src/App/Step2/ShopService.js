export default class ShopService{

  sendData(formData) {
    return fetch(`http://172.16.10.170:8080/shop`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "currency": formData.currency,
        "lang": formData.language,
        "name": formData.shop_name,
        "tz": formData.timezone,
        "url": formData.shop_url,
        "userName": formData.name
      })
    });
  }
}
