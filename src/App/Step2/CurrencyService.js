export default class CurrencyService{
  async getList() {
    return fetch(process.env.PUBLIC_URL + `data/currencies.json`)
      .then(response => response.json());
  }
}
