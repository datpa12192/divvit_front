import React from "react";

export default class LanguageService {
  async getList() {
    return fetch(process.env.PUBLIC_URL + `data/languages.json`)
      .then(response => response.json());
  }
}
