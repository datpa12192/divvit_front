export default class TimezoneService {
  async getList() {
    return fetch('http://api.timezonedb.com/v2.1/list-time-zone?key=9Y01XHVFFCQJ&format=json')
      .then(response => response.json())
      .then(timezones => timezones.zones.map(timezone => {
        return {
          value: timezone.zoneName,
          label: timezone.zoneName
        }
      }));
  }
}
