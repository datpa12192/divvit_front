export default class Form3Validation {

  validation(formData) {
    const errors = {};
    if (formData.platform === '') {
      errors.platform = {
        type: true,
        message: 'Required'
      }
    } else {
      delete errors.platform;
    }
    return errors;

  }
}
