import React from "react";
import {Grid, Card, CardActionArea, CardMedia, CardContent, Typography} from "@material-ui/core";
import ButtonBack from "../Button/ButtonBack";
import ButtonContinue from "../Button/ButtonContinue";

export default class Form3YourShopPlatform extends React.Component {

  render() {
    const {formData, classes, activeStep, handleBack, handleSubmit, handleChangePlatform, errors} = this.props;

    return (
      <form action="">
        <Grid container>
          <Grid item xs={4}>
            <Card
              className={formData.platform === 'magento' ? classes.cardSelected : ''}
              style={{width: '90%'}}
              onClick={() => handleChangePlatform('magento')}
            >
              <CardActionArea style={{height: '70px'}}>
                <CardMedia
                  component="img"
                  alt="Contemplative Reptile"
                  height="auto"
                  image={process.env.PUBLIC_URL + '/img/magento.png'}
                  title="Contemplative Reptile"
                />
              </CardActionArea>
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card
              className={formData.platform === 'shopware' ? classes.cardSelected : ''}
              style={{width: '90%'}}
              onClick={() => handleChangePlatform('shopware')}
            >
              <CardActionArea style={{height: '70px'}}>
                <CardMedia
                  component="img"
                  alt="Contemplative Reptile"
                  height="auto"
                  image={process.env.PUBLIC_URL + '/img/shopware.png'}
                  title="Contemplative Reptile"
                />
              </CardActionArea>
            </Card>
          </Grid>
          <Grid item xs={4}>
            <Card
              className={formData.platform === 'other' ? classes.cardSelected : ''}
              style={{width: '90%'}}
              onClick={() => handleChangePlatform('other')}
            >
              <CardActionArea style={{height: '70px'}}>
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Orther
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Grid>
        </Grid>

        <div className={typeof errors.platform !== "undefined" ? 'text-red' : 'hidden'}>
          {errors.platform && errors.platform.message}
        </div>

        <div>
          <ButtonBack
            activeStep={activeStep}
            classes={classes}
            handleBack={handleBack}
          />

          <ButtonContinue
            activeStep={activeStep}
            classes={classes}
            handleSubmit={handleSubmit}
          />
        </div>
      </form>
    );
  }
}
