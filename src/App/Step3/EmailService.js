export default class EmailService{

  sendData(formData) {
    return fetch(`http://172.16.10.170:8080/email`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"mailTo": formData.email, "platformId": formData.platform})
    });
  }
}
