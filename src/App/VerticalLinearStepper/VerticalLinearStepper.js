import React from "react";
import {withStyles, createMuiTheme, ThemeProvider, withTheme} from "@material-ui/core/styles";
import blue from "@material-ui/core/colors/blue";
import {Stepper, Step, StepLabel, StepContent, Paper, Typography, Button} from "@material-ui/core";
import Form1AboutYourSelf from "../Step1/Form1AboutYourSelf";
import Form2AboutYourShop from "../Step2/Form2AboutYourShop";
import Form3YourShopPlatform from "../Step3/Form3YourShopPlatform";
import UserService from "../Step1/UserService";
import ShopService from "../Step2/ShopService";
import EmailService from "../Step3/EmailService";
import moment from "moment";
import "moment-timezone";
import LocaleCurrency from "locale-currency";
import Form1Validation from "../Step1/Form1Validation"
import Form2Validation from "../Step2/Form2Validation"
import Form3Validation from "../Step3/Form3Validation"

const theme = createMuiTheme({
  palette: {
    primary: blue,
  }
});

const styles = {
  root: {
    width: '50%',
    margin: '0px auto'
  },
  button: {
    marginTop: ({theme}) => theme.spacing(1),
    marginRight: ({theme}) => theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: ({theme}) => theme.spacing(2)
  },
  resetContainer: {
    padding: ({theme}) => theme.spacing(3)
  },
  cardSelected: {
    border: '1px solid green',
  },
  palette: {
    primary: {
      light: '#008bc5',
      main: '#008bc5',
      dark: '#303f9f',
      contrastText: '#fff'
    }
  }
};

const form1Validation = new Form1Validation();
const form2Validation = new Form2Validation();
const form3Validation = new Form3Validation();

class VerticalLinearStepper extends React.Component {

  constructor() {
    super();
    const userLang = navigator.language || navigator.userLanguage;

    this.state = {
      activeStep: 0,
      formData: {
        name: '',
        email: '',
        function: '',
        shop_name: '',
        shop_url: '',
        currency: LocaleCurrency.getCurrency(userLang),
        language: moment.locale(),
        timezone: moment.tz.guess(),
        platform: ''
      },
      errors: {}
    }
  };



  handleSubmit = () => {

    var errors = {};
    const formData = this.state.formData;

    switch (this.state.activeStep) {
      case 0:
        errors = form1Validation.validation(formData);
        break;
      case 1:
        errors = form2Validation.validation(formData);
        break;
      case 2:
        errors = form3Validation.validation(formData);
        break;
      default:
        break;
    }

    this.setState({errors: errors});

    if (JSON.stringify(errors) === '{}') {
      switch (this.state.activeStep) {
        case 0:
          new UserService().sendData(formData).then(() => {
            this.setState({activeStep: this.state.activeStep + 1});
          }).catch(() => {
            alert("Something went wrong!");
          });

          break;
        case 1:
          new ShopService().sendData(formData).then(() => {
            this.setState({activeStep: this.state.activeStep + 1});
          }).catch(() => {
            alert("Something went wrong!");
          });
          break;
        case 2:
          new EmailService().sendData(formData).then(() => {
            this.setState({activeStep: this.state.activeStep + 1});
          }).catch(() => {
            alert("Something went wrong!");
          });
          break;
        default:
          break;
      }
    }

  };

  handleBack = () => {
    this.setState({activeStep: this.state.activeStep - 1});
  };

  handleChangeData = (event) => {
    const formData = this.state.formData;

    this.setState({
      formData: {
        name: event.target.name === 'name' ? event.target.value : formData.name,
        email: event.target.name === 'email' ? event.target.value : formData.email,
        function: event.target.name === 'function' ? event.target.value : formData.function,
        currency: event.target.name === 'currency' ? event.target.value : formData.currency,
        language: event.target.name === 'language' ? event.target.value : formData.language,
        timezone: event.target.name === 'timezone' ? event.target.value : formData.timezone,
        shop_name: event.target.name === 'shop_name' ? event.target.value : formData.shop_name,
        shop_url: event.target.name === 'shop_url' ? event.target.value : formData.shop_url,
        platform: event.target.name === 'platform' ? event.target.value : formData.platform,
      }
    });
  };

  handleChangePlatform = (platform) => {
    const formData = this.state.formData;
    formData.platform = platform;

    this.setState({
      formData: formData
    });
  };

  render() {
    const classes = this.props.classes;
    const {activeStep, formData, errors} = this.state;

    return (
      <ThemeProvider theme={theme}>
        <div className={classes.root}>
          <h1 className="header">Divvit</h1>
          <Stepper activeStep={activeStep} orientation="vertical">
            <Step>
              <StepLabel>
                About Your Self
              </StepLabel>
              <StepContent orientation="vertical">
                <Form1AboutYourSelf
                  formData={formData}
                  activeStep={activeStep}
                  classes={classes}
                  handleSubmit={this.handleSubmit}
                  handleBack={this.handleBack}
                  handleChangeData={this.handleChangeData}
                  errors={errors}
                ></Form1AboutYourSelf>
              </StepContent>
            </Step>

            <Step>
              <StepLabel>
                About Your Shop
              </StepLabel>
              <StepContent orientation="vertical">
                <Form2AboutYourShop
                  formData={formData}
                  activeStep={activeStep}
                  classes={classes}
                  handleSubmit={this.handleSubmit}
                  handleBack={this.handleBack}
                  handleChangeData={this.handleChangeData}
                  errors={errors}
                />
              </StepContent>
            </Step>

            <Step>
              <StepLabel>
                Your Shop Platform
              </StepLabel>
              <StepContent orientation="vertical">
                <Form3YourShopPlatform
                  formData={formData}
                  activeStep={activeStep}
                  classes={classes}
                  handleSubmit={this.handleSubmit}
                  handleBack={this.handleBack}
                  handleChangePlatform={this.handleChangePlatform}
                  errors={errors}
                />
              </StepContent>
            </Step>
          </Stepper>

          {activeStep === 3 && (
            <Paper square elevation={0} className={classes.resetContainer}>
              <Typography>All steps completed - you&apos;re finished</Typography>
              <Typography>We will send you setup instructions via email</Typography>
              <div>
                <pre>
                  {JSON.stringify(formData, null, 2)}
                </pre>
              </div>
              <Button onClick={() => this.setState({activeStep: 0})} className={classes.button}>
                Reset
              </Button>
            </Paper>
          )}
        </div>
      </ThemeProvider>
    );
  }
}

export default withTheme(withStyles(styles)(VerticalLinearStepper));
