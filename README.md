## Divvit Test Project

### Installation

```reactjs
    npm install
```

### Build

```reactjs
    npm build
```

### Nginx setup

```nginx
    server {
        listen  #port
        server_name #domain
        root #/var/www/build/index.html
    }

```
